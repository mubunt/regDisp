# *regDisp*, a generic & configurable tool to display configuration registers.
***
In this document, and in the application it describes, a register is a hardware or software user-configurable 32-bit register that determines which specific system parameters are set during initialization. The configuration of each parameter is a set of bits named field in the remainder of this document.


The **regDisp** (Register Display) is a C application running on Linux and Windows. This application is customized for any set of registers that the user wishes to display. To do this, it must update the following 2 source files:


- *src/regDisp.h*: This header file contains the structures defining each register in terms of name and bit fields with their possible values.
- *src/regDisp.ggo*: This file defines the **regDisp** execution options and needs to be updated to include options for displaying new registers.


These 2 files already contain the definitions of 3 registers: *my_First_Register*, *my_Second_Register* and *my_Third_Register*. These data are to be replaced by the data of the new registers.

- *src/regDisp.h*:
   - Structures *my1stReg*, *my2ndReg* and *my3rdReg*.
   - The macro *REGISTER_SELECTION* allowing the association directive between option and structure.
   - The string *title* to display a general title for each register.
- *src/regDisp.ggo*:
   - Définition of new options in *groupoption* directives (one per option).


Of course, the application will probably be renamed to be more user-friendly.

## LICENSE
**regDisp** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ regDisp --help
regDisp 1.0.0

A generic & configurable tool to display configuration registers

Usage: regDisp [OPTIONS]... [DIRECTORY]...

  -h, --help                    Print help and exit
  -V, --version                 Print version and exit
  -l, --long                    Long display mode.  (default=off)
  -n, --noframe                 Display without frame  (default=off)

Exit: returns a non-zero status if an error is detected.


 Group: REGISTERS
  Set of configuration registers
      --firstReg=<Hexadecimal bit field>
                                Display bit field for my1stReg Configuration
                                  register
      --secondReg=<Hexadecimal bit field>
                                Display bit field for my2ndReg Configuration
                                  register
      --thirdReg=<Hexadecimal bit field>
                                Display bit field for my3rdReg Configuration
                                  register
$ 
```
**Example 1:** 
![regDisp](README_images/regDisp-01.png  "regDisp")
**Example 2:**
![regDisp](README_images/regDisp-02.png  "regDisp")

## STRUCTURE OF THE APPLICATION
This section walks you through **regDisp**'s structure. Once you understand this structure, you will easily find your way around in **regDisp**'s code base.
``` bash
$ yaTree
./                     # Application level
├── README_images/     # Images for documentation
│   ├── regDisp-01.png # -- Screenshot yaTree on Ubuntu/terminator
│   └── regDisp-02.png # -- Screenshot yaTree on Ubuntu/terminator
├── src/               # Source directory
│   ├── Makefile       # -- Makefile
│   ├── gframe.c       # -- C source file to manage display
│   ├── gframe.h       # -- C Header file with 'gPrint.c' function prototypes
│   ├── regDisp.c      # -- C main source file
│   ├── regDisp.ggo    # -- 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   └── regDisp.h      # -- C Header file with data common to all '.c' files
├── COPYING.md         # GNU General Public License markdown file
├── LICENSE.md         # License markdown file
├── Makefile           # Top-level makefile
├── README.md          # ReadMe Mark-Down file
├── RELEASENOTES.md    # Release Notes markdown file
└── VERSION            # Version identification text file

2 directories, 14 files
$
```	

## HOW TO BUILD THIS APPLICATION
### Linux Platform
``` bash
$ cd regDisp
$ make clean all
```
### Windows Platform
For the Windows platform, the application is built on Linux (cross-platform generation).
``` bash
$ cd regDisp
$ make wclean wall
```
## HOW TO INSTALL AND USE THIS APPLICATION
### Linux Platform
``` bash
$ cd regDisp
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```
### Windows Platform
Install *regDisp.exe* a mano.

## SOFTWARE REQUIREMENTS
- For usage:
   - Nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - MinGW-64 packages for Windows cross-generation
- Application developed and tested with  XUBUNTU 18.04
- Until version 1.2.0, application  tested with WINDOWS 10.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .
***
