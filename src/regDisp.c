//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: regDisp
// A generic & configurable tool to display configuration registers.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef LINUX
#include <linux/limits.h>
#include <unistd.h>
#else
#include <limits.h>
#endif
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "regDisp_cmdline.h"
#include "gframe.h"
#include "regDisp.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define error(fmt, ...) \
	do { fprintf(stderr, "ERROR: " fmt "\n", __VA_ARGS__); } while (0)
// Example: error("Error at line=%d", n);
//			error("%s", "No valide argument");
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;

//------------------------------------------------------------------------------
// INTERNAL FUNCTIONS
//------------------------------------------------------------------------------
static int getConfigValue(char *param, unsigned int *config) {
	char *pt;
	unsigned int tmp;

	*config = 0;
	pt = param;;
	if ((strlen(pt) <= 2) || strlen(pt) > 10) goto ERR;
	if (*pt++ != '0') goto ERR;
	if (*pt++ != 'x') goto ERR;
	while (*pt != '\0') {
		*pt = *pt & 0x7F;
		if ((*pt >= 0x30) && (*pt <= 0x39))
			tmp = (unsigned int) *pt - 0x30;
		else {
			if ((*pt >= 0x41) && (*pt <= 0x46))
				tmp = (unsigned int) *pt - 0x37;
			else {
				if ((*pt >= 0x61) && (*pt <= 0x66))
					tmp = (unsigned int) *pt - 0x57;
				else
					goto ERR;
			}
		}
		*config = *config << 4 | tmp;
		pt++;
	}
	return(EXIT_SUCCESS);
ERR:
	return(EXIT_FAILURE);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *hexaTitle(unsigned int x, char *so) {
	char t[9];

	sprintf(t, "%08X", x);
	so[0] = '\0';
	for (int i = 0; i < 8; i++) {
		strcat(so, "   ");
		size_t  j = strlen(so);
		so[j++] = t[i];
		so[j] = '\0';
	}
	return(so);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *binaryTitle(unsigned int x, char *so) {
	char s[33];
	int i = 31;

	strcpy(s, "00000000000000000000000000000000");
	do {
		s[i--] = (x & 1) ? '1':'0';
		x >>= 1;
	} while (i >= 0);
	sprintf(so, "%s", s);
	return(so);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static unsigned int computeMask(short start, short length) {
	unsigned int mask = 0;
	for (int i = 0; i < length; i++) mask = mask << 1 | 1;
	for (int i = 0; i < start; i++) mask = mask << 1;;
	return mask;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *printBitField(unsigned int x, short start, short length, char *so) {
	char s[33];
	int i = 31;
	int decode = 0;

	strcpy(s, "00000000000000000000000000000000");
	do {
		if (i == (31 - start)) decode++;
		if (decode) s[i] = (x & 1) ? '1':'0';
		else s[i] = '.';
		if (i == (31 - (start + length - 1))) decode--;
		i--;
		x >>= 1;
	} while( i >= 0);
	sprintf(so, "%s", s);
	return(so);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void ssbinaryString(unsigned int number, char *so) {
	if (number) {
		ssbinaryString(number >> 1, so);
		strcat(so, (number & 1) ? "1" : "0");
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *binaryString(unsigned int number, short nbits, char *so) {
	char stmp[33];
	stmp[0] = '\0';
	if (! number) strcat(stmp, "0");
	else ssbinaryString(number, stmp);

	so[0] = '\0';
	for (int i = 0; i < nbits; i++) strcat(so, "0");
	so[strlen(so) - strlen(stmp)] = '\0';
	strcat(so, stmp);
	return so;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void decodeRegister(struct s_register *ptReg, unsigned int config) {
	char stmp2[64];
	char stmp1[35];
	sprintf(stmp2, "%%-%ds%%s - ", MAXFIELDNAME + 2);

	int i = 0;
	while (ptReg->registerFields[i].fieldName[0] != 0) {
		unsigned int tmp = config & computeMask(ptReg->registerFields[i].startBit, ptReg->registerFields[i].lengthBit);
		gframe_add(stmp2, ptReg->registerFields[i].fieldName,
		           printBitField(tmp, ptReg->registerFields[i].startBit, ptReg->registerFields[i].lengthBit, stmp1));
		tmp = tmp >> ptReg->registerFields[i].startBit;
		int j = 0;
		while (ptReg->registerFields[i].setOfValues[j].litteral[0] != 0) {
			if (ptReg->registerFields[i].setOfValues[j].value == tmp) break;
			++j;
		}
		if (ptReg->registerFields[i].setOfValues[j].litteral[0] != 0)
			gframe_add("%s\n", ptReg->registerFields[i].setOfValues[j].litteral);
		else {
			if (strcmp(ptReg->registerFields[i].fieldName, RESERVED) == 0) {
				gframe_add("%s\n", "RESERVED FIELD. THE VALUE 0 WOULD BE MORE APPROPRIATE");
			} else {
				gframe_add("%s\n", "WRONG VALUE");
			}
		}
		if (args_info.long_given && strcmp(ptReg->registerFields[i].fieldName, RESERVED) != 0) {
			char stmp3[64];
			sprintf(stmp3, "%%-%ds0b%%s %%s\n", MAXFIELDNAME + 39);
			j = 0;
			while (ptReg->registerFields[i].setOfValues[j].litteral[0] != 0) {
				gframe_add(stmp3,
				           " ",
				           binaryString(ptReg->registerFields[i].setOfValues[j].value, ptReg->registerFields[i]. lengthBit, stmp1),
				           ptReg->registerFields[i].setOfValues[j].litteral);
				++j;
			}
		}
		++i;
	}
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct s_register *ptRegister;
	unsigned int config;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_regDisp(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;

	char *parameter;

	REGISTER_SELECTION;
	if (ptRegister == NULL) {
		error("%s", "Cannot determine which register to decode and display.");
		goto Error;
	}
	if (getConfigValue(parameter, &config) == EXIT_FAILURE) {
		error("Bad hexadecimal value (%s)", parameter);
		goto Error;
	}
	//--- Processing -----------------------------------------------------------
	gframe_ini();
	gframe_add("%s - REGISTER %s=0x%08X\n\n", title, ptRegister->registerName, config);
	char stmp1[33];
	char stmp2[64];
	sprintf(stmp2, "%%%ds%%s\n", MAXFIELDNAME + 2);
	gframe_add(stmp2, "0x", hexaTitle(config, stmp1));
	gframe_add(stmp2, "0b", binaryTitle(config,stmp1));
	decodeRegister(ptRegister, config);
	if (args_info.noframe_given) {
		gframe_close(stdout, NOFRAME);
	} else {
		gframe_close(stdout, SINGLEFRAME);
	}
	//--- Exit -----------------------------------------------------------------
Exit:
	cmdline_parser_regDisp_free(&args_info);
	return EXIT_SUCCESS;
Error:
	cmdline_parser_regDisp_free(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
