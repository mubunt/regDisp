//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: regDisp
// A generic & configurable tool to display configuration registers.
//------------------------------------------------------------------------------

/**
 *------------------------------------------------------------------------------
 * SYSTEM HEADER FILES
 *------------------------------------------------------------------------------
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#ifdef LINUX
#include <sys/ioctl.h>
#else
#include <windows.h>
#endif
/**
 *------------------------------------------------------------------------------
 * APPLICATION HEADER FILE
 *------------------------------------------------------------------------------
 */
#include "gframe.h"
/**
 *------------------------------------------------------------------------------
 * MACROS DEFINITIONS
 *------------------------------------------------------------------------------
 */
#define TERM						"TERM"
#define MSYSCON						"MSYSCON"
#define ERROR_PREFIX				"GFRAME ERROR"

#define MAX(a,b)					(((a)>(b))?(a):(b))

// XTERM-LIKE GRAPHIC CHARSET
#define BEGCOLOR 				"\033["
#define ENDCOLOR				'm'

#define XBOLD						"\033[1m"
#define XRESET						"\033[0m"
#define XGRAPHMODEON				"\033(0"
#define XGRAPHMODEOFF				"\033(B"
#define XSRBC						106	// Single Right Bottom Corner
#define XSRUC						107	// Single Right Up Corner
#define XSLUC						108	// Single Left Up Corner
#define XSLBC						109	// Single Left Bottom Corner
#define	XSH							113	// Single Horizontal
#define XSV							120	// Single Vertical
// WINDOWS CMD GRAPHIC CHARSET
// https://en.wikipedia.org/wiki/Code_page_437
#define SV							179	// Single Vertical
#define DV							186	// Double Vertical
#define DRUC						187	// Double Right Up Corner
#define DRBC						188	// Double Right Bottom Corner
#define SRUC						191	// Single Right Up Corner
#define SLBC						192	// Single Left Bottom Corner
#define	SH							196	// Single Horizontal
#define DLBC						200	// Double Left Bottom Corner
#define DLUC						201	// Double Left Up Corner
#define DH							205	// Double Horizontal
#define SRBC						217	// Single Right Bottom Corner
#define SLUC						218	// Single Left Up Corner
/**
 *------------------------------------------------------------------------------
 * ENUMERATION
 *------------------------------------------------------------------------------
 */
typedef enum {
	XTERM,
	XTERMWIN,
	CMDWIN
} eTerminalType;
/**
 *------------------------------------------------------------------------------
 * GLOBAL VARIABLES
 *------------------------------------------------------------------------------
 */
static FILE 			*__fdbuff		= NULL;
static size_t			__width			= 0;
static size_t			__currentWidth 	= 0;
static eTerminalType	__terminalType	= XTERM;
static char			tmpname[64];
/**
 *------------------------------------------------------------------------------
 * INTERNAL FUNCTIONS
 *------------------------------------------------------------------------------
 */
void __gframe_basicDisplayError(char *label) {
	fprintf(stderr, "\n%s: %s\n\n", ERROR_PREFIX, label);
}

void __gframe_displayError(char *label, eTerminalType format) {
	unsigned int i;

	// Default to do graphical output when connected to a TTY but don't output the
	// escape sequences when connected to a file

	switch (format) {
	case CMDWIN:		// Windows command line and Windows/Bash
		if (isatty(2)) {
			fprintf(stderr, "\n %c", SLUC);
			fprintf(stderr, " %s ", ERROR_PREFIX);
			for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", SH);
			fprintf(stderr, "%c\n", SRUC);
			fprintf(stderr, " %c %s %c\n", SV, label, SV);
			fprintf(stderr, " %c", SLBC);
			for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", SH);
			fprintf(stderr, "%c\n\n", SRBC);
		} else
			__gframe_basicDisplayError(label);
		break;
	case XTERM:			// Linux (XTERM)
		if (isatty(2)) {
			fprintf(stderr, "\n %s%c%s", XGRAPHMODEON, XSLUC, XGRAPHMODEOFF);
			fprintf(stderr, " %s%s%s ", XBOLD, ERROR_PREFIX, XRESET);
			fprintf(stderr, "%s", XGRAPHMODEON);
			for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", XSH);
			fprintf(stderr, "%c%s\n", XSRUC, XGRAPHMODEOFF);
			fprintf(stderr, " %s%c%s %s %s%c%s\n", XGRAPHMODEON, XSV, XGRAPHMODEOFF, label, XGRAPHMODEON, XSV, XGRAPHMODEOFF);
			fprintf(stderr, " %s%c", XGRAPHMODEON, XSLBC);
			for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", XSH);
			fprintf(stderr, "%c%s\n", XSRBC, XGRAPHMODEOFF);
		} else
			__gframe_basicDisplayError(label);
		break;
	case XTERMWIN:		// and MinGW shell
		// Be careful: isatty does not run on MinGW!!!
		fprintf(stderr, "\n %s%c%s", XGRAPHMODEON, XSLUC, XGRAPHMODEOFF);
		fprintf(stderr, " %s%s%s ", XBOLD, ERROR_PREFIX, XRESET);
		fprintf(stderr, "%s", XGRAPHMODEON);
		for (i = 0; i < (strlen(label) - strlen(ERROR_PREFIX)); i++) fprintf(stderr, "%c", XSH);
		fprintf(stderr, "%c%s\n", XSRUC, XGRAPHMODEOFF);
		fprintf(stderr, " %s%c%s %s %s%c%s\n", XGRAPHMODEON, XSV, XGRAPHMODEOFF, label, XGRAPHMODEON, XSV, XGRAPHMODEOFF);
		fprintf(stderr, " %s%c", XGRAPHMODEON, XSLBC);
		for (i = 0; i < (strlen(label) + 2); i++) fprintf(stderr, "%c", XSH);
		fprintf(stderr, "%c%s\n", XSRBC, XGRAPHMODEOFF);
		break;
	}
}

void __gframe_header(FILE *fdout, size_t width, eOutputFrame mode) {
	eOutputFrame mode_reworked;
	unsigned int i;

	mode_reworked = mode;
	if ((mode_reworked == DOUBLEFRAME) && (__terminalType != CMDWIN))
		mode_reworked = SINGLEFRAME;

	switch (mode_reworked) {
	case NOFRAME:
		// nothing to do
		fprintf(fdout, "\n");
		break;
	case TEXTFRAME:
		fprintf(stdout, "\n %c", '+');
		for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", '-');
		fprintf(stdout, "%c\n", '+');
		break;
	case SINGLEFRAME:
		if (__terminalType == CMDWIN) {
			fprintf(stdout, "\n %c", SLUC);
			for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", SH);
			fprintf(stdout, "%c\n", SRUC);
		} else {
			fprintf(stdout, "\n %s%c", XGRAPHMODEON, XSLUC);
			for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", XSH);
			fprintf(stdout, "%c%s\n", XSRUC, XGRAPHMODEOFF);
		}
		break;
	case DOUBLEFRAME:
		fprintf(stdout, "\n %c", DLUC);
		for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", DH);
		fprintf(stdout, "%c\n", DRUC);
		break;
	}
}

void __gframe_line(FILE *fdout, size_t width, eOutputFrame mode, char *buff) {
	eOutputFrame mode_reworked;
	size_t i;

	mode_reworked = mode;
	if ((mode_reworked == DOUBLEFRAME) && (__terminalType != CMDWIN))
		mode_reworked = SINGLEFRAME;

	switch (mode_reworked) {
	case NOFRAME:
		fprintf(fdout, "%s\n", buff);
		break;
	case TEXTFRAME:
		fprintf(fdout, " %c %s", '|', buff);
		for (i = width; i < __width; i++) fprintf(stdout, " ");
		fprintf(stdout, " %c\n", '|');
		break;
	case SINGLEFRAME:
		if (__terminalType == CMDWIN) {
			fprintf(fdout, " %c %s", SV, buff);
			for (i = width; i < __width; i++) fprintf(stdout, " ");
			fprintf(stdout, " %c\n", SV);
		} else {
			fprintf(stdout, " %s%c%s %s", XGRAPHMODEON, XSV, XGRAPHMODEOFF, buff);
			for (i = width; i < __width; i++) fprintf(stdout, " ");
			fprintf(stdout, " %s%c%s\n", XGRAPHMODEON, XSV, XGRAPHMODEOFF);
		}
		break;
	case DOUBLEFRAME:
		fprintf(fdout, " %c %s", DV, buff);
		for (i = width; i < __width; i++) fprintf(stdout, " ");
		fprintf(stdout, " %c\n", DV);
		break;
	}
}

void __gframe_trailer(FILE *fdout, size_t width, eOutputFrame mode) {
	eOutputFrame mode_reworked;
	unsigned int i;

	mode_reworked = mode;
	if ((mode_reworked == DOUBLEFRAME) && (__terminalType != CMDWIN))
		mode_reworked = SINGLEFRAME;

	switch (mode_reworked) {
	case NOFRAME:
		// nothing to do
		fprintf(fdout, "\n");
		break;
	case TEXTFRAME:
		fprintf(stdout, " %c", '+');
		for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", '-');
		fprintf(stdout, "%c\n\n", '+');
		break;
	case SINGLEFRAME:
		if (__terminalType == CMDWIN) {
			fprintf(stdout, " %c", SLBC);
			for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", SH);
			fprintf(stdout, "%c\n\n", SRBC);
		} else {
			fprintf(stdout, " %s%c", XGRAPHMODEON, XSLBC);
			for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", XSH);
			fprintf(stdout, "%c%s\n\n", XSRBC, XGRAPHMODEOFF);
		}
		break;
	case DOUBLEFRAME:
		fprintf(stdout, " %c", DLBC);
		for (i = 0; i < (width + 2); i++) fprintf(stdout, "%c", DH);
		fprintf(stdout, "%c\n\n", DRBC);
		break;
	}
}
//-----------------------------------------------------------------------------
static eTerminalType __gframe_getTerminalType() {
	char *pt;

	if (NULL == getenv(TERM))					// Windows Command Line
		return(CMDWIN);
	if ((pt = getenv(MSYSCON)) == NULL)			// Linux shell
		return(XTERM);
	if (strcmp(pt, "sh.exe") == 0)				// Windows/Bash shell
		return(CMDWIN);
	return(XTERMWIN);
}

static void __gframe_error(const char *label, ...) {
	va_list al;
	char buff[512];

	va_start(al, label);
	vsprintf(buff, label, al);
	va_end(al);

	if (__terminalType == CMDWIN) {
		__gframe_displayError(buff, CMDWIN);
	} else {								// Linux, MinGW or Windows/Bash shell
		if (__terminalType == XTERM) {		// Linux shell
			__gframe_displayError(buff, XTERM);
		} else {
			__gframe_displayError(buff, XTERMWIN);
		}
	}
	fflush(stderr);
	exit(-1);
}
/**
 *------------------------------------------------------------------------------
 * GFRAME_INI
 *------------------------------------------------------------------------------
 */
void gframe_ini() {
	// Type of terminal (xterm, xterm-windows, cmd-windows)
	__terminalType = __gframe_getTerminalType();
	// Temporary file creation
	sprintf(tmpname, "./__gframe%d", getpid());
	if (NULL == (__fdbuff = fopen(tmpname, "w+"))) {
		__gframe_error("%s", strerror(errno));
	}
	// Some initializations
	__currentWidth = __width = 0;
}
/**
 *------------------------------------------------------------------------------
 * GFRAME_ADD
 *------------------------------------------------------------------------------
 */
void gframe_add(const char *format, ...) {
	va_list al;
	char buff[2048];
	char *pt;
	size_t n, i, k, tobesubstrated;

	if (__fdbuff == NULL) {
		__gframe_error("Package not initialized");
	}
	va_start(al, format);
	vsprintf(buff, format, al);
	va_end(al);

	n = fwrite(buff, 1, strlen(buff), __fdbuff);
	pt = buff;
	k = tobesubstrated = 0;
	for (i = 0; i < n; i++) {
		if (buff[i] == '\n') {
			k = i;
			buff[i] = '\0';
			__width = MAX(__width, (__currentWidth + strlen(pt) - tobesubstrated));
			tobesubstrated = __currentWidth = 0;
			pt = buff + i + 1;
		} else {
			if ((__terminalType == XTERM) || (__terminalType == XTERMWIN)) {
				char *ptmp;
				size_t k;
				ptmp = buff + i;
				if ((strncmp(ptmp, XBOLD, strlen(XBOLD)) == 0) ||
				        (strncmp(ptmp, XRESET, strlen(XRESET)) == 0)) {
					tobesubstrated = tobesubstrated + strlen(XBOLD);
				} else {
					if ((strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) ||
					        (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0)) {
						tobesubstrated = tobesubstrated + strlen(XGRAPHMODEON);
					} else {
						if (strncmp(ptmp, BEGCOLOR, strlen(BEGCOLOR)) == 0)  {
							ptmp = ptmp + strlen(BEGCOLOR);
							for (k = 0; k < 5; k++) {
								if (*ptmp != '\0') ++ptmp;
							}
							if (*ptmp == ENDCOLOR) {
								tobesubstrated = tobesubstrated + strlen(BEGCOLOR) + 6;
							} else continue;
						} else continue;
					}
				}
			}
		}
	}
	if (k != n - 1) {
		__currentWidth = __currentWidth + (n - k);
	}
}
/**
 *------------------------------------------------------------------------------
 * GFRAME_CLOSE
 *------------------------------------------------------------------------------
 */
void gframe_close(FILE *fdout, eOutputFrame mode) {
	size_t n;
	char line[2018];

	rewind(__fdbuff);

	__gframe_header(fdout, __width, mode);
	while ((fgets(line, sizeof(line), __fdbuff)) != NULL) {
		n = strlen(line);
		if (line[n - 1] == '\n') {
			line[n - 1] = '\0';
			--n;
		}
		if ((__terminalType == XTERM) || (__terminalType == XTERMWIN)) {
			size_t i, k, tobesubstrated;
			char *ptmp;

			tobesubstrated = 0;
			for (i = 0; i < n; i++) {
				ptmp = line + i;
				if ((strncmp(ptmp, XBOLD, strlen(XBOLD)) == 0) ||
				        (strncmp(ptmp, XRESET, strlen(XRESET)) == 0)) {
					tobesubstrated = tobesubstrated + strlen(XBOLD);
				} else {
					if ((strncmp(ptmp, XGRAPHMODEON, strlen(XGRAPHMODEON)) == 0) ||
					        (strncmp(ptmp, XGRAPHMODEOFF, strlen(XGRAPHMODEOFF)) == 0)) {
						tobesubstrated = tobesubstrated + strlen(XGRAPHMODEON);
					} else {
						if (strncmp(ptmp, BEGCOLOR, strlen(BEGCOLOR)) == 0)  {
							ptmp = ptmp + strlen(BEGCOLOR);
							ptmp = ptmp + strlen(BEGCOLOR);
							for (k = 0; k < 5; k++) {
								if (*ptmp != '\0') ++ptmp;
							}
							if (*ptmp == ENDCOLOR) {
								tobesubstrated = tobesubstrated + strlen(BEGCOLOR) + 6;
							} else continue;
						} else continue;
					}
				}
			}
			n = n - tobesubstrated;
		}
		__gframe_line(fdout, n, mode, line);
	}
	__gframe_trailer(fdout, __width, mode);
	(void) fclose(__fdbuff);
	(void)unlink(tmpname);
	__fdbuff = NULL;
}
/**
 *------------------------------------------------------------------------------
 * GFRAME_ABORT
 *------------------------------------------------------------------------------
 */
void gframe_abort() {
	if (__fdbuff != NULL) {
		(void) fclose(__fdbuff);
	}
	(void)unlink(tmpname);
	__fdbuff = NULL;
}
#ifdef LINUX
/**
 *------------------------------------------------------------------------------
 * GFRAME_TESTWIDTH
 *
 * Result:	0: The current window has not a sufficient width for the log
 *			1: The current window has a sufficient width for the log
 *------------------------------------------------------------------------------
 */
int gframe_testWidth(eOutputFrame mode) {
	struct winsize ws;
	ioctl(1, TIOCGWINSZ, &ws);
	if (mode == NOFRAME)
		return(__width <= ws.ws_col ? 1 : 0);
	else
		return((__width + 4) <= ws.ws_col ? 1 : 0);
}
#endif

#ifdef TEST
/**
 *------------------------------------------------------------------------------
 * AUTO-TEST MODE
 *------------------------------------------------------------------------------
 */
int main(void) {
	int i, k;

	/*
		fprintf(stdout, "TEST #A (test 'error')\n");
		gframe_ini();
		__gframe_error("%s", strerror(10));	gframe_close(stdout, NOFRAME);
	*/
	/*
		fprintf(stdout, "TEST #B (test 'error')\n");
		gframe_add("%s", "AAAAAAAAAAA");
	*/
	fprintf(stdout, "TEST #1\n");
	gframe_ini();
	for (i = 0; i < 20; i++) gframe_add("%s\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	gframe_add("%s\n", "bbbbbb");
#ifdef LINUX
	k = gframe_testWidth(NOFRAME);
#endif
	gframe_close(stdout, NOFRAME);
#ifdef LINUX
	if (k) fprintf(stdout, "The window is wide enough to display\n\n");
	else fprintf(stdout, "The window is NOT wide enough to display\n\n");
#endif

	fprintf(stdout, "TEST #2\n");
	gframe_ini();
	for (i = 0; i < 20; i++) gframe_add("%s\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	gframe_add("%s\n", "bbbbbb");
#ifdef LINUX
	k = gframe_testWidth(TEXTFRAME);
#endif
	gframe_close(stdout, TEXTFRAME);
#ifdef LINUX
	if (k) fprintf(stdout, "The window is wide enough to display\n\n");
	else fprintf(stdout, "The window is NOT wide enough to display\n\n");
#endif

	fprintf(stdout, "TEST #3\n");
	gframe_ini();
	for (i = 0; i < 20; i++) gframe_add("%s\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	gframe_add("%s\n\n", "aaaaaaaaaaaaaaaaaaaaaa");
	gframe_add("%s\n", "bbbbbb");
#ifdef LINUX
	k = gframe_testWidth(SINGLEFRAME);
#endif
	gframe_close(stdout, SINGLEFRAME);
#ifdef LINUX
	if (k) fprintf(stdout, "The window is wide enough to display\n\n");
	else fprintf(stdout, "The window is NOT wide enough to display\n\n");
#endif
	fprintf(stdout, "TEST #4\n");
	gframe_ini();
	for (i = 0; i < 20; i++) gframe_add("%s\n", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	gframe_add("%s\n", "bbbbbb");
#ifdef LINUX
	k = gframe_testWidth(DOUBLEFRAME);
#endif
	gframe_close(stdout, DOUBLEFRAME);
#ifdef LINUX
	if (k) fprintf(stdout, "The window is wide enough to display\n\n");
	else fprintf(stdout, "The window is NOT wide enough to display\n\n");
#endif

	fprintf(stdout, "TEST #5\n");
	gframe_ini();
	for (i = 0; i < 10; i++) gframe_add("%s\n", "abcdefghijklmn");
	gframe_add("%s%s%c%s%s%c%s%s\n", "abc", XGRAPHMODEON, XSV, XGRAPHMODEOFF, XGRAPHMODEON, XSV, XGRAPHMODEOFF, "fghijklmn");
	for (i = 0; i < 10; i++) gframe_add("%s\n", "abcdefghijklmn");
	gframe_close(stdout, SINGLEFRAME);

	fprintf(stdout, "TEST #6\n");
	gframe_ini();
	gframe_add("    _/_/_/_/_/_/_/\n");
	gframe_add("   _/       _/      STxP70 Toolset %-36s\n", "2022.4");
	gframe_add("    _/_/   _/       Linux-based command-line environment\n");
	gframe_add("     _/   _/\n");
	gframe_add("_/_/_/   _/\n");
	gframe_close(stdout, SINGLEFRAME);

	fprintf(stdout, "TEST #7\n");
	gframe_ini();
	gframe_add("%s    _/_/_/_/_/_/_/%s\n", XBOLD, XRESET);
	gframe_add("%s   _/       _/      STxP70 Toolset %-36s %s\n", XBOLD, "2022.4", XRESET);
	gframe_add("%s    _/_/   _/       Linux-based command-line environment%s\n", XBOLD, XRESET);
	gframe_add("%s     _/   _/%s\n", XBOLD, XRESET);
	gframe_add("%s_/_/_/   _/%s\n", XBOLD, XRESET);
	gframe_close(stdout, SINGLEFRAME);

	fprintf(stdout, "TEST #8\n");
	gframe_ini();
	gframe_add("%s%s    _/_/_/_/_/_/_/                                                      %s\n", "\033[37;44m", XBOLD, XRESET);
	gframe_add("%s%s   _/       _/      STxP70 Toolset %-36s %s\n", "\033[37;44m", XBOLD, "2022.4", XRESET);
	gframe_add("%s%s    _/_/   _/       Linux-based command-line environment                %s\n", "\033[37;44m", XBOLD, XRESET);
	gframe_add("%s%s     _/   _/                                                            %s\n", "\033[37;44m", XBOLD, XRESET);
	gframe_add("%s%s_/_/_/   _/                                                             %s\n", "\033[37;44m", XBOLD, XRESET);
	gframe_close(stdout, SINGLEFRAME);
}
#endif
