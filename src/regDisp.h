//------------------------------------------------------------------------------
// Copyright (c) 2017-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: regDisp
// A generic & configurable tool to display configuration registers.
//------------------------------------------------------------------------------
#ifndef REGDISP_H
#define REGDISP_H

//------------------------------------------------------------------------------
// CONSTANT DEFINITIONS - To be customized if needed...
//------------------------------------------------------------------------------
#define MAXFIELDS			17			// Maximal number of fiels for all registers.
#define MAXVALUESPERFIELD	33			// Maximal number of values dor a field.
#define MAXFIELDNAME		8			// Maximal number of chars of a field name.
#define RESERVED			"Reserved"	// Key word for reserved bit field
//------------------------------------------------------------------------------
// STRUCTURE DEFINITIONS - Do not modify a priori
//------------------------------------------------------------------------------
struct s_values {
	unsigned int 	value;
	const char 		*litteral;
};
struct s_bitfields {
	const char 		*fieldName;
	short			startBit;
	short			lengthBit;
	struct s_values	setOfValues[MAXVALUESPERFIELD];
	// Note: All the possibilities (2 power n, if n is the number of bits of the field)
	// are not necessarily valid. Holes may therefore exist.
};
struct s_register {
	const char 			*registerName;
	struct s_bitfields	registerFields[MAXFIELDS];
};
//------------------------------------------------------------------------------
// GLOBAL VARIABLES - To be customized....
//------------------------------------------------------------------------------
// Global title to display for each register
const char *title = "PROCESSOR VERCORS17";
// Define as many variables of this type as there are registers to be decoded and display.
// Hereafter, my1stReg and my2ndReg are examples.
struct s_register my1stReg = {
	.registerName="my_First_Register",
	.registerFields={
		{
			.fieldName="CTXT", .startBit=0, .lengthBit=2,
			.setOfValues={
				{ 0b00, "1 context" },
				{ 0b01, "2 contexts" },
				{ 0b10, "4 contexts" },
				{ 0b11, "8 contexts" },
				{ 0, "" }
			}
		},
		{
			.fieldName="BKBCT", .startBit=2, .lengthBit=1,
			.setOfValues={
				{ 0b0, "1 register bank per context" },
				{ 0b1, "2 register banks per context"},
				{ 0, "" }
			}
		},
		{
			.fieldName="MP", .startBit=3, .lengthBit=1,
			.setOfValues={ { 0b0, "Multiplier not implemented in X3" },
				{ 0b1, "Multiplier implemented in X3" },
				{ 0, "" }
			}
		},
		{
			.fieldName="EFU", .startBit=4, .lengthBit=3,
			.setOfValues={
				{ 0b000, "No EFU interface" },
				{ 0b001, "32-bit EFU interface" },
				{ 0b010, "64-bit EFU interface" },
				{ 0b011, "128-bit EFU interface" },
				{ 0b100, "256-bit EFU interface" },
				{ 0b101, "512-bit EFU interface" },
				{ 0, "" }
			}
		},
		{
			.fieldName="MFU", .startBit=7, .lengthBit=3,
			.setOfValues={
				{ 0b000, "No MFU interface" },
				{ 0b001, "32-bit MFU interface" },
				{ 0b010, "64-bit MFU interface" },
				{ 0b011, "128-bit MFU interface" },
				{ 0b100, "256-bit MFU interface" },
				{ 0b101, "512-bit MFU interface" },
				{ 0, "" }
			}
		},
		{
			.fieldName="EXM", .startBit=10, .lengthBit=2,
			.setOfValues={
				{ 0b00, "No external memory interface" },
				{ 0b01, "32-bit external memory interface" },
				{ 0b10, "64-bit external memory interface" },
				{ 0, "" }
			}
		},
		{
			.fieldName="ITC", .startBit=12, .lengthBit=3,
			.setOfValues={
				{ 0b000, "No Interrupt Controller" },
				{ 0b001, "7+1 ITC nodes (7 maskable interrupts + NMI)" },
				{ 0b010, "15+1 ITC nodes (15 maskable interrupts + NMI)" },
				{ 0b011, "31+1 ITC nodes (31 maskable interrupts + NMI)" },
				{ 0, "" }
			}
		},
		{
			.fieldName="EVC", .startBit=15, .lengthBit=5,
			.setOfValues={
				{ 0b00000, "No event controller" },
				{ 0b00001, "4 global + 4 local events" },
				{ 0b00010, "No event controller" },
				{ 0b00011, "8 global + 4 local events" },
				{ 0b00100, "No event controller" },
				{ 0b00101, "16 global + 4 local events" },
				{ 0b00110, "No event controller" },
				{ 0b00111, "32 global + 4 local events" },
				{ 0b01000, "No event controller" },
				{ 0b01001, "4 global + 8 local events" },
				{ 0b01010, "No event controller" },
				{ 0b01011, "8 global + 8 local events" },
				{ 0b01100, "No event controller" },
				{ 0b01101, "16 global + 8 local events" },
				{ 0b01110, "No event controller" },
				{ 0b01111, "32 global + 8 local events"  },
				{ 0b10000, "No event controller" },
				{ 0b10001, "4 global + 16 local events" },
				{ 0b10010, "No event controller" },
				{ 0b10011, "8 global + 16 local events" },
				{ 0b10100, "No event controller" },
				{ 0b10101, "16 global + 16 local events" },
				{ 0b10110, "No event controller" },
				{ 0b10111, "32 global + 16 local events" },
				{ 0b11000, "No event controller" },
				{ 0b11001, "4 global + 32 local events" },
				{ 0b11010, "No event controller" },
				{ 0b11011, "8 global + 32 local events" },
				{ 0b11100, "No event controller" },
				{ 0b11101, "16 global + 32 local events" },
				{ 0b11110, "No event controller" },
				{ 0b11111, "32 global + 32 local events" },
				{ 0, "" }
			}
		},
		{
			.fieldName="HLO", .startBit=20, .lengthBit=1,
			.setOfValues={
				{ 0b0, "No hardware loop" },
				{ 0b1, "2 hardware loops per context" },
				{ 0, "" }
			}
		},
		{
			.fieldName=RESERVED, .startBit=21, .lengthBit=1,
			.setOfValues={
				{ 0b0, RESERVED },
				{ 0, "" }
			}
		},
		{
			.fieldName="DMS", .startBit=22, .lengthBit=4,
			.setOfValues={
				{ 0b0000, "No internal data memory" },
				{ 0b0001, "TCDM size = 512 Bytes" },
				{ 0b0010, "TCDM size = 1 K-Bytes" },
				{ 0b0011, "TCDM size = 2 K-Bytes" },
				{ 0b0100, "TCDM size = 4 K-Bytes" },
				{ 0b0101, "TCDM size = 8 K-Bytes" },
				{ 0b0110, "TCDM size = 16 K-Bytes" },
				{ 0b0111, "TCDM size = 32 K-Bytes" },
				{ 0b1000, "TCDM size = 64 K-Bytes" },
				{ 0b1001, "TCDM size = 128 K-Bytes" },
				{ 0b1010, "TCDM size = 256 K-Bytes" },
				{ 0b1011, "TCDM size = 512 K-Bytes" },
				{ 0b1100, "TCDM size = 1 M-Bytes" },
				{ 0b1101, "TCDM size = 2 M-Bytes" },
				{ 0b1110, "TCDM size = 4 M-Bytes" },
				{ 0, "" }
			}
		},
		{
			.fieldName="DCACHE", .startBit=26, .lengthBit=1,
			.setOfValues={
				{ 0b0, "Data Cache not implemented" },
				{ 0b01, "Data Cache implemented" },
				{ 0, "" }
			}
		},
		{
			.fieldName="PMS", .startBit=27, .lengthBit=4,
			.setOfValues={
				{ 0b0000, "No internal program memory" },
				{ 0b0010, "TCPM size = 1 K-Bytes" },
				{ 0b0011, "TCPM size = 2 K-Bytes" },
				{ 0b0100, "TCPM size = 4 K-Bytes" },
				{ 0b0101, "TCPM size = 8 K-Bytes" },
				{ 0b0110, "TCPM size = 16 K-Bytes" },
				{ 0b0111, "TCPM size = 32 K-Bytes" },
				{ 0b1000, "TCPM size = 64 K-Bytes" },
				{ 0b1001, "TCPM size = 128 K-Bytes" },
				{ 0b1010, "TCPM size = 256 K-Bytes" },
				{ 0b1011, "TCPM size = 512 K-Bytes" },
				{ 0b1100, "TCPM size = 1 M-Bytes" },
				{ 0b1101, "TCPM size = 2 M-Bytes" },
				{ 0b1110, "TCPM size = 4 M-Bytes" },
				{ 0, "" }
			}
		},
		{
			.fieldName="PCACHE", .startBit=31, .lengthBit=1,
			.setOfValues={
				{ 0b0, "Program Cache not implemented" },
				{ 0b1, "Program Cache implemented" },
				{ 0, "" }
			}
		},
		{
			.fieldName=""
		}
	}
};
struct s_register my2ndReg = {
	.registerName="my_Second_Register",
	.registerFields={
		{
			.fieldName=RESERVED, .startBit=0,  .lengthBit=3,
			.setOfValues={
				{ 0b000, RESERVED },
				{ 0, "" }
			}
		},
		{
			.fieldName="ROMPEN", .startBit=3,  .lengthBit=1,
			.setOfValues={
				{ 0b0, "ROM patch controller not supported" },
				{ 0b1, "ROM patch controller supported" },
				{ 0, "" }
			}
		},
		{
			.fieldName="MAXSZMIS", .startBit=4,  .lengthBit=3,
			.setOfValues={
				{ 0b000, "No misalignment supported" },
				{ 0b001, "Up to 2-Byte misaligned access" },
				{ 0b010, "Up to 4-Byte misaligned access" },
				{ 0b011, "Up to 8-Byte misaligned access" },
				{ 0b100, "Up to 16-Byte misaligned access" },
				{ 0b101, "Up to 32-Byte misaligned access" },
				{ 0b110, "Up to 64-Byte misaligned access" },
				{ 0, "" }
			}
		},
		{
			.fieldName="MINADMIS", .startBit=7,  .lengthBit=3,
			.setOfValues={
				{ 0b000, "Down to 1-Byte address alignment for misaligned access" },
				{ 0b001, "Down to 2-Byte address alignment for misaligned access" },
				{ 0b010, "Down to 4-Byte address alignment for misaligned access" },
				{ 0b011, "Down to 8-Byte address alignment for misaligned access" },
				{ 0b100, "Down to 16-Byte address alignment for misaligned access" },
				{ 0b101, "Down to 32 Byte address alignment for misaligned access" },
				{ 0, "" }
			}
		},
		{
			.fieldName="VLIWEN", .startBit=10, .lengthBit=2,
			.setOfValues={
				{ 0b00, "Dual-issue not supported" },
				{ 0b11, "Dual-issue with optional core ALU" },
				{ 0, "" }
			}
		},
		{
			.fieldName="MPU", .startBit=12, .lengthBit=2,
			.setOfValues={
				{ 0b00, "MPU not supported" },
				{ 0b01, "8-region MPU" },
				{ 0b10, "12-region MPU" },
				{ 0b11, "16-region MPU" },
				{ 0, "" }
			}
		},
		{
			.fieldName="WP", .startBit=14, .lengthBit=1,
			.setOfValues={
				{ 0b0, "Watch-points not supported" },
				{ 0b1, "4 watch-points supported" },
				{ 0, "" }
			}
		},
		{
			.fieldName=RESERVED, .startBit=15, .lengthBit=1,
			.setOfValues={
				{ 0b00, RESERVED },
				{ 0, "" }
			}
		},
		{
			.fieldName="USER", .startBit=16, .lengthBit=4,
			.setOfValues={
				{ 0b0000, "Standard configuration" },
				{ 0b0001, "User specific configuration #1" },
				{ 0b0010, "User specific configuration #2" },
				{ 0b0011, "User specific configuration #3" },
				{ 0b0100, "User specific configuration #4" },
				{ 0b0101, "User specific configuration #5" },
				{ 0b0110, "User specific configuration #6" },
				{ 0b0111, "User specific configuration #7" },
				{ 0b1000, "User specific configuration #8" },
				{ 0b1001, "User specific configuration #9" },
				{ 0b1010, "User specific configuration #10" },
				{ 0b1011, "User specific configuration #11" },
				{ 0b1100, "User specific configuration #12" },
				{ 0b1101, "User specific configuration #13" },
				{ 0b1110, "User specific configuration #14" },
				{ 0b1111, "User specific configuration #15" },
				{ 0, "" }
			}
		},
		{
			.fieldName="BHB", .startBit=20, .lengthBit=2,
			.setOfValues={
				{ 0b00, "Branch Target Buffer with 1 entry." },
				{ 0b001, "Branch History Buffer with 8 target addresses entries." },
				{ 0, "" }
			}
		},
		{
			.fieldName="BYPASS", .startBit=22, .lengthBit=2,
			.setOfValues={
				{ 0b00, "No extra bypass supported." },
				{ 0b01, "MEM2-EXE bypass is supported." },
				{ 0, "" }
			}
		},
		{
			.fieldName="NEXUS", .startBit=24, .lengthBit=1,
			.setOfValues={
				{ 0b0, "Nexus trace i/f not supported."},
				{ 0b1, "Nexus trace i/f supported"},
				{ 0, "" }
			}
		},
		{
			.fieldName="DAPB", .startBit=25, .lengthBit=1,
			.setOfValues={
				{ 0b0, "Debug trough JTAG"},
				{ 0b1, "Debug Trough Advanced Peripheral Bus"},
				{ 0, "" }
			}
		},
		{
			.fieldName="EXTM_AXI", .startBit=26, .lengthBit=1,
			.setOfValues={
				{ 0b0, "EXTM is STBUS"},
				{ 0b1, "EXTM is AXI"},
				{ 0, "" }
			}
		},
		{
			.fieldName="MFU_AXI", .startBit=27, .lengthBit=1,
			.setOfValues={
				{ 0b0, "MFU is STBUS"},
				{ 0b1, "MFU is AXI"},
				{ 0, "" }
			}
		},
		{
			.fieldName=RESERVED, .startBit=28, .lengthBit=4,
			.setOfValues={
				{ 0b0000, RESERVED },
				{ 0, "" }
			}
		},
		{
			.fieldName=""
		}
	}
};
struct s_register my3rdReg = {
	.registerName="my_Third_Register",
	.registerFields={
		{
			.fieldName="MAXSZMIS", .startBit=0,  .lengthBit=3,
			.setOfValues={
				{ 0b000, "No misalignment supported" },
				{ 0b001, "Up to 2-Byte misaligned access" },
				{ 0b010, "Up to 4-Byte misaligned access" },
				{ 0b011, "Up to 8-Byte misaligned access" },
				{ 0b100, "Up to 16-Byte misaligned access" },
				{ 0b101, "Up to 32-Byte misaligned access" },
				{ 0b110, "Up to 64-Byte misaligned access" },
				{ 0b111, "Up to 128-Byte misaligned access" },
				{ 0, "" }
			}
		},
		{
			.fieldName="ROMPEN", .startBit=3,  .lengthBit=1,
			.setOfValues={
				{ 0b0, "ROM patch controller not supported" },
				{ 0b1, "ROM patch controller supported" },
				{ 0, "" }
			}
		},
		{
			.fieldName=RESERVED, .startBit=4,  .lengthBit=28,
			.setOfValues={
				{ 0b000, RESERVED },
				{ 0, "" }
			}
		},
		{
			.fieldName=""
		}
	}
};
//------------------------------------------------------------------------------
// GLOBAL MACROS - To be customized....
//------------------------------------------------------------------------------
// Selection og the register to be displayed.
#define REGISTER_SELECTION		if (args_info.firstReg_given) { \
									ptRegister = &my1stReg; \
									parameter = args_info.firstReg_arg; \
								} else { \
									if (args_info.secondReg_given) { \
										ptRegister = &my2ndReg; \
										parameter = args_info.secondReg_arg; \
									} else { \
										if (args_info.thirdReg_given) { \
											ptRegister = &my3rdReg; \
											parameter = args_info.thirdReg_arg; \
										} else \
											ptRegister = NULL; \
									} \
								}

#endif	// REGDISP_H
//------------------------------------------------------------------------------
