# RELEASE NOTES: *regDisp*, a generic & configurable tool to display configuration registers.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.2.10**:
  - Updated build system components.

- **Version 1.2.9**:
  - Updated build system.

- **Version 1.2.8**:
  - Removed unused files.

- **Version 1.2.7**:
  - Updated build system component(s)

- **Version 1.2.6**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.2.5**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.2.4**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.2.3:**
  - Added tagging of new release.

- **Version 1.2.2:**
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.2.1:**
  - Fixed typos in README.md file.

- **Version 1.2.0:**
  - Improved "clean" target in str/Makefile.
  - Formatting and improving the README file.
  - Added ".comment" file in each directory for 'yaTree' utility.
  - Added ".gitignore" file.

- **Version 1.1.0:**
  - Added ".comment" file in each directory for 'yaTree' utility.

- **Version 1.0.0:**
  - First release.
